#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/sabnzbd-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	export USER_ID=$(id -u)
	export GROUP_ID=$(id -g)
	echo Setting up nsswrapper mapping `id -u` to sabnzbd
	(
	    cat /etc/passwd
	    echo "sabnzbd:x:$USER_ID:$GROUP_ID::/app/sabnzbd:/bin/sh"
	) >/tmp/sabnzbd-passwd
	(
	    cat /etc/group
	    echo "sabnzbd:x:$GROUP_ID:"
	) >/tmp/sabnzbd-group
    fi
    export NSS_WRAPPER_PASSWD=/tmp/sabnzbd-passwd
    export NSS_WRAPPER_GROUP=/tmp/sabnzbd-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
