#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/nsswrapper.sh
. /usr/local/bin/reset-tls.sh

if test -s /datadir/config.ini; then
    if test -z "$API_KEY"; then
	if grep ^api_key /datadir/config.ini >/dev/null; then
	    API_KEY=`awk '/api_key = /{print $1;exit}' /datadir/config.ini`
	fi
    fi
    if test -z "$NZB_KEY"; then
	if grep ^nzb_key /datadir/config.ini >/dev/null; then
	    NZB_KEY=`awk '/nzb_key = /{print $1;exit}' /datadir/config.ini`
	fi
    fi
    if test "$DO_NOT_RESET"; then
	if ! grep -E '^__version__ = 19' /datadir/config.ini >/dev/null; then
	    DO_NOT_RESET=
	fi
    fi
else
    DO_NOT_RESET=
fi

if test -z "$DO_NOT_RESET"; then
    HOSTLIST="$SABNAME"
    LOCALE=${LOCALE:-en}
    NNTP_HOSTNAME=${NNTP_HOSTNAME:-reader.usenetbucket.com}
    NNTP_MAXCONN=${NNTP_MAXCONN:-20}
    NNTP_PASS=${NNTP_PASS:-secret}
    NNTP_PORT=${NNTP_PORT:-443}
    NNTP_TLS=${NNTP_TLS:-1}
    NNTP_USER=${NNTP_USER:-sab}
    SABNAME=${SABNAME:-sab.demo.local}
    if test "$SMTP_RELAY"; then
	if test -z "$SMTP_TO"; then
	    SMTP_TO=changeme@demo.local
	fi
	if test -z "$SMTP_FROM"; then
	    SMTP_FROM=sab@$SABNAME
	fi
	SMTP_PORT=${SMTP_PORT:-:25}
	SMTP_PORT=:$SMTP_PORT
    fi
    if test "$DISABLE_AUTH"; then
	SABPASS=
	SABUSER=
    else
	SABPASS="${SABPASS:-secret}"
	SABUSER=${SABUSER:-sab}
    fi
    if test "$HOSTALIASES"; then
	for a in $HOSTALIASES
	do
	    HOSTLIST="$HOSTLIST, $a"
	done
    fi
    if test -z "$API_KEY"; then
	API_KEY=`cat /dev/urandom | tr -dc 'a-f0-9' | fold -w 32 | head -n 1`
    fi
    if test -z "$NZB_KEY"; then
	NZB_KEY=`cat /dev/urandom | tr -dc 'a-f0-9' | fold -w 32 | head -n 1`
    fi
    sed -e "s|APIKEY|$API_KEY|" \
	-e "s|HOSTLIST|$HOSTLIST|" \
	-e "s|LANG|$LOCALE|" \
	-e "s|NNTP_HOSTNAME|$NNTP_HOSTNAME|" \
	-e "s|NNTP_MAXCONN|$NNTP_MAXCONN|" \
	-e "s|NNTP_PASS|$NNTP_PASS|" \
	-e "s|NNTP_PORT|$NNTP_PORT|" \
	-e "s|NNTP_TLS|$NNTP_TLS|" \
	-e "s|NNTP_USER|$NNTP_USER|" \
	-e "s|NZBKEY|$NZB_KEY|" \
	-e "s|SABUSER|$SABUSER|" \
	-e "s|SABPASS|$SABPASS|" \
	-e "s|SMTP_FROM|$SMTP_FROM|" \
	-e "s|SMTP_PORT|$SMTP_PORT|" \
	-e "s|SMTP_RELAY|$SMTP_RELAY|" \
	-e "s|SMTP_TO|$SMTP_TO|" \
	/config.ini >/datadir/config.ini
fi

mkdir -p /datadir/bin /datadir/logs /datadir/Downloads \
    /datadir/Downloads/incomplete /datadir/Downloads/completed

rm -f /datadir/logs/* >/dev/null 2>&1 || echo no logs to reset
ln -sf /dev/stdout /datadir/logs/sabnzbd.log

cat /usr/local/bin/post_process_all >/datadir/bin/post_process_all
chmod +x /datadir/bin/post_process_all

unset SABUSER SABPASS API_KEY NZB_KEY HOSTLIST SABNAME HOSTALIASES \
    DO_NOT_RESET

cd /sabnzbd
exec ./SABnzbd.py -b 0 -f /datadir/config.ini -s 0.0.0.0:8080
