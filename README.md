# k8s SABnzbd

SABnzbd image.

Build with:

```
$ make build
```

Test with:

```
$ make demo
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name   |    Description             | Default                       |
| :----------------- | -------------------------- | ----------------------------- |
|  `API_KEY`         | SABnzbd API Key            | generated during startup      |
|  `DISABLE_AUTH`    | Disables SABnzbd Auth      | undef                         |
|  `DO_NOT_RESET`    | Disables Config Generation | undef                         |
|  `HOSTLIST`        | SABnzbd Site FQDN Aliases  | undef                         |
|  `LOCALE`          | SABnzbd Language           | `en`                          |
|  `NNTP_HOSTNAME`   | SABnzbd NNTP Provider      | `reader.usenetbucket.com`     |
|  `NNTP_MAXCONN`    | SABnzbd NNTP Max Conn.     | `20`                          |
|  `NNTP_PASS`       | SABnzbd NNTP Password      | `secret`                      |
|  `NNTP_PORT`       | SABnzbd NNTP Port          | `443`                         |
|  `NNTP_TLS`        | SABnzbd NNTP uses TLS      | `1`                           |
|  `NNTP_USER`       | SABnzbd NNTP Username      | `sab`                         |
|  `NZB_KEY`         | SABnzbd NZB Key            | generated during startup      |
|  `SABNAME`         | SABnzbd Site FQDN          | `sab.demo.local`              |
|  `SABPASS`         | SABnzbd Admin Password     | `secret`                      |
|  `SABUSER`         | SABnzbd Admin Username     | `sab`                         |
|  `SLACK_HOOK_URL`  | Slack notifications URL    | undef                         |
|  `SMTP_FROM`       | SABnzbd SMTP From          | `sab@$SABNAME`                |
|  `SMTP_PORT`       | SABnzbd SMTP Port          | `25`                          |
|  `SMTP_RELAY`      | SABnzbd SMTP Relay         | undef                         |
|  `SMTP_TO`         | SABnzbd SMTP Recipient     | `changeme@demo.local`         |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description             |
| :------------------ | ----------------------- |
|  `/datadir`         | SABnzbd data directory  |
