FROM docker.io/python:3-slim-buster

# SABnzbd image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    LANGUAGE=en_US:en \
    LANG=en_US.UTF-8 \
    LC_ALL=en_US.UTF-8 \
    SAB_REPO=https://github.com/sabnzbd/sabnzbd \
    SAB_VERSION=3.6.1

LABEL io.k8s.description="SABnzbd $SAB_VERSION Image." \
      io.k8s.display-name="SABnzbd $SAB_VERSION" \
      io.openshift.expose-services="8081:http" \
      io.openshift.tags="sab,sabnzbd" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-sabnzbd" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$SAB_VERSION"

COPY config/* /

RUN set -x \
    && apt-get update \
    && echo "# Installing Dumb-init" \
    && apt-get install -y dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && mv /nsswrapper.sh /reset-tls.sh /usr/local/bin/ \
    && echo "# Installing SABnzbd Dependencies" \
    && apt-get install --no-install-recommends -y p7zip-full par2 unrar-free \
	unzip openssl git ca-certificates locales locales-all libnss-wrapper \
	rsync gcc musl-dev libffi-dev libc6-dev \
    && if ! grep "^# ${LANG}" /etc/locale.gen >/dev/null; then \
	export LANG=en_US.UTF-8; \
    fi \
    && if ! grep "^$LANG" /etc/locale.gen >/dev/null; then \
	echo "$LANG" >>/etc/locale.gen; \
    fi \
    && touch /usr/share/locale/locale.alias \
    && locale-gen \
    && echo export LANG=${LANG} >>/etc/default/locale \
    && echo "# Installing SABnzbd" \
    && git clone -b $SAB_VERSION $SAB_REPO /sabnzbd \
    && mkdir -p /datadir /media \
    && python3 -m pip install -r /sabnzbd/requirements.txt \
    && ln -sf /usr/local/bin/python3 /usr/bin/ \
    && ln -sf /usr/local/bin/python /usr/bin/ \
    && mv /post_process_all /slack /usr/local/bin/ \
    && echo "# Fixing permissions" \
    && for dir in /datadir /media /sabnzbd; \
	do \
	    chown -R 1001:root "$dir" \
	    && chmod -R g=u "$dir"; \
	done \
    && echo "# Cleaning Up" \
    && apt-get remove -y --purge git gcc musl-dev libffi-dev libc6-dev \
    && apt-get autoremove -y --purge \
    && apt-get clean \
    && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/* /tmp/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init","--","/run-sabnzbd.sh" ]
USER 1001
