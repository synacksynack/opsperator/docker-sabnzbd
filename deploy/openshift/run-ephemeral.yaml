apiVersion: v1
kind: Template
labels:
  app: sabnzbd
  template: sabnzbd-ephemeral
message: |-
  The following service(s) have been created in your project:
      https://sabnzbd.${ROOT_DOMAIN} -- SABnzbd
metadata:
  annotations:
    description: SABnzbd - ephemeral
    iconClass: icon-python
    openshift.io/display-name: SABnzbd
    tags: sabnzbd
  name: sabnzbd-ephemeral
objects:
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      name: sabnzbd-${FRONTNAME}
    name: sabnzbd-${FRONTNAME}
  spec:
    replicas: 1
    selector:
      name: sabnzbd-${FRONTNAME}
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: sabnzbd-${FRONTNAME}
      spec:
        dnsPolicy: ClusterFirst
        containers:
        - env:
          - name: API_KEY
            valueFrom:
              secretKeyRef:
                key: api-key
                name: sabnzbd-${FRONTNAME}
          - name: LOCALE
            value: en
          - name: NZB_KEY
            valueFrom:
              secretKeyRef:
                key: nzb-key
                name: sabnzbd-${FRONTNAME}
          - name: SABNAME
            value: sabnzbd.${ROOT_DOMAIN}
          - name: SABPASS
            valueFrom:
              secretKeyRef:
                key: admin-password
                name: sabnzbd-${FRONTNAME}
          - name: SABUSER
            valueFrom:
              secretKeyRef:
                key: admin-user
                name: sabnzbd-${FRONTNAME}
          - name: SMTP_FROM
            value: sab@${ROOT_DOMAIN}
          - name: SMTP_RELAY
            value: ${SMTP_RELAY}
          - name: SMTP_TO
            value: admin0@${ROOT_DOMAIN}
          - name: TZ
            value: Europe/Paris
          imagePullPolicy: IfNotPresent
          livenessProbe:
            initialDelaySeconds: 30
            httpGet:
              path: /
              port: 8080
            periodSeconds: 20
            timeoutSeconds: 8
          name: sabnzbd
          ports:
          - containerPort: 8080
            protocol: TCP
          readinessProbe:
            initialDelaySeconds: 5
            httpGet:
              path: /
              port: 8080
            periodSeconds: 20
            timeoutSeconds: 5
          resources:
            limits:
              cpu: "${SABNZBD_CPU_LIMIT}"
              memory: "${SABNZBD_MEMORY_LIMIT}"
          volumeMounts:
          - name: data
            mountPath: /datadir
        - env:
          - name: AUTH_METHOD
            value: none
          - name: PROXY_HTTP_PORT
            value: "8082"
          - name: PROXY_SERVER_NAME
            value: sab-completed.${ROOT_DOMAIN}
          - name: PUBLIC_PROTO
            value: https
          - name: SERVE_DIRECTORY
            value: /var/www/html
          - name: TZ
            value: Europe/Paris
          imagePullPolicy: IfNotPresent
          livenessProbe:
            failureThreshold: 30
            httpGet:
              path: /
              port: 8082
            initialDelaySeconds: 30
            periodSeconds: 20
            successThreshold: 1
            timeoutSeconds: 8
          name: authproxy
          ports:
          - containerPort: 8082
            protocol: TCP
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /
              port: 8082
            initialDelaySeconds: 5
            periodSeconds: 20
            successThreshold: 1
            timeoutSeconds: 5
          resources:
            limits:
              cpu: 50m
              memory: 256Mi
            requests:
              cpu: 10m
              memory: 128Mi
          volumeMounts:
          - mountPath: /etc/apache2/sites-enabled
            name: temp
            subPath: apache
          - mountPath: /var/www/html
            name: data
        restartPolicy: Always
        volumes:
        - emptyDir: {}
          name: data
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - sabnzbd
        from:
          kind: ImageStreamTag
          name: sabnzbd:${SABNZBD_IMAGE_TAG}
    - type: ConfigChange
- apiVersion: v1
  kind: Service
  metadata:
    name: sabnzbd-${FRONTNAME}-downloads
  spec:
    ports:
    - name: http
      port: 8082
    selector:
      name: sabnzbd-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    name: sabnzbd-${FRONTNAME}-downloads
  spec:
    host: sab-completed.${ROOT_DOMAIN}
    to:
      kind: Service
      name: sabnzbd-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
- apiVersion: v1
  kind: Service
  metadata:
    name: sabnzbd-${FRONTNAME}
  spec:
    ports:
    - name: http
      port: 8080
    selector:
      name: sabnzbd-${FRONTNAME}
- apiVersion: v1
  kind: Route
  metadata:
    name: sabnzbd-${FRONTNAME}
  spec:
    host: sabnzbd.${ROOT_DOMAIN}
    to:
      kind: Service
      name: sabnzbd-${FRONTNAME}
    tls:
      insecureEdgeTerminationPolicy: Redirect
      termination: edge
parameters:
- name: FRONTNAME
  description: The name identifier assigned to objects defined in this template
  displayName: Name
  required: true
  value: demo
- name: SABNZBD_CPU_LIMIT
  description: Maximum amount of CPU a SABnzbd container can use
  displayName: Lemon CPU Limit
  required: true
  value: 300m
- name: SABNZBD_IMAGE_TAG
  description: The ImageStreamTag we should pull images from
  displayName: Tag
  required: true
  value: master
- name: SABNZBD_MEMORY_LIMIT
  description: Maximum amount of memory a SABnzbd container can use
  displayName: Lemon Memory Limit
  required: true
  value: 1Gi
- name: ROOT_DOMAIN
  description: Root Domain
  displayName: Root Domain
  required: true
  value: demo.local
- name: SMTP_RELAY
  description: SMTP Relay
  displayName: SMTP Relay
  required: true
  value: smtp.demo.local
